//
//  CustomPlayerNode.swift
//  MP3PlayApp
//
//  Copyright © 2019 Agency Canoe. All rights reserved.
//

import UIKit
import AVFoundation

class CustomPlayerNode {
    var player: AVAudioPlayerNode
    var audioFile: AVAudioFile?
    var tag: Int
    var isAttached: Bool
    var volume: Float
    
    init(player: AVAudioPlayerNode, audioFile: AVAudioFile? = nil, tag: Int, isAttached: Bool, volume: Float = 0.85) {
        self.player = player
        self.audioFile = audioFile
        self.tag = tag
        self.isAttached = isAttached
        self.volume = volume
    }
    
}
