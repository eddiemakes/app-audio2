//
//  MainTVC.swift
//  Record Twice
//
//  Copyright © 2019 Agency Canoe. All rights reserved.
//

import UIKit
import AVFoundation
import AudioUnit

class MainTVC: UITableViewController, AVAudioRecorderDelegate, AVAudioPlayerDelegate {

    @IBOutlet weak var buttonPlayPause: UIButton!
    @IBOutlet weak var buttonStop: UIButton!
    @IBOutlet weak var buttonRecordTrack1: UIButton!
    @IBOutlet weak var buttonRecordTrack2: UIButton!
    @IBOutlet weak var labelTrack1: UILabel!
    @IBOutlet weak var labelTrack2: UILabel!
    
    
    var audioEngine: AVAudioEngine = AVAudioEngine()
    var mixerNode: AVAudioMixerNode = AVAudioMixerNode()
    let playerNode1: AVAudioPlayerNode = AVAudioPlayerNode()
    let playerNode2: AVAudioPlayerNode = AVAudioPlayerNode()
    var recorder: AVAudioRecorder?
    var audioSession: AVAudioSession!

    var audioTracks = ["track1", "track2"]
    var currentAudioTrack = 0

    var playerNodePaused: Bool = false
    var track1Recording: Bool = false
    var track2Recording: Bool = false
    var playAllPlaying: Bool = false
    var playAllPlayingFinished1: Bool = true
    var playAllPlayingFinished2: Bool = true
    
    
    // Audio settings, format and channels
    // iOS on most devices only supports 1 channel of recording
    let settings = [
        AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
        AVSampleRateKey: 12000,
        AVNumberOfChannelsKey: 1,
        AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
    ]
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkMicrophonePermission()
        initInterface()
        deleteAllTracks()
    }
    
    // MARK: - Setup UI
    
    // Configures interface (shadows, colors, etc...)
    func initInterface() {
        buttonPlayPause.layer.cornerRadius = 20
        buttonPlayPause.layer.shadowOpacity = 1
        buttonPlayPause.layer.shadowOffset = CGSize(width: 0, height: 0)
        buttonPlayPause.layer.shadowRadius = 50
        buttonPlayPause.layer.shadowColor = UIColor(red: 0, green: 122/255, blue: 1, alpha: 1).cgColor
        
        buttonStop.layer.cornerRadius = 20
        buttonStop.layer.shadowOpacity = 1
        buttonStop.layer.shadowOffset = CGSize(width: 0, height: 0)
        buttonStop.layer.shadowRadius = 50
        buttonStop.layer.shadowColor = UIColor(red: 1, green: 0.1, blue: 0.1, alpha: 1).cgColor
    }
    
    // Enables the top buttons and set greater hue
    func enablePlayStop() {
        buttonStop.isEnabled = true
        buttonPlayPause.isEnabled = true
        buttonStop.alpha = 1
        buttonPlayPause.alpha = 1
    }
    
    // Disables the top buttons and sets lower hue
    /// <#Description#>
    func disablePlayStop() {
        buttonPlayPause.setTitle("PLAY", for: .normal)
        buttonStop.isEnabled = false
        buttonPlayPause.isEnabled = false
        buttonStop.alpha = 0.3
        buttonPlayPause.alpha = 0.3
    }
    

    // MARK: - Audio Setup
    
    func audioSetup() {
        // We get the main mixer from iOS for adding other functionalities to it
        mixerNode = audioEngine.mainMixerNode
        
        audioEngine.attach(playerNode1)
        audioEngine.attach(playerNode2)
        
        // then here we connect both players to main mixer we got before
        audioEngine.connect(playerNode1, to: mixerNode, format: mixerNode.outputFormat(forBus: 0))
        audioEngine.connect(playerNode2, to: mixerNode, format: mixerNode.outputFormat(forBus: 0))
    }
    
    // A function where we pass as argument the track name and the audio player
    // on which the track will be played
   
    func prepareAudioPlay(track: String, audioPlayerNode: AVAudioPlayerNode) {
        var audioFile: AVAudioFile?
        do {
            try audioFile = AVAudioFile(forReading: getFileUrl(file: track))
            audioPlayerNode.scheduleFile(audioFile!, at: nil, completionHandler: nil)
            audioPlayerNode.play(at: nil)
        }catch let error {
            print("Error opening audio file: \(error.localizedDescription)")
        }
    }
    
    // A function that prepares and starts the audio engine
    func prepareStartEngine() {
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch let error {
            print("Error starting audio engine: \(error.localizedDescription)")
        }
    }
    
    
    // MARK: - Audio Record
    
    // A function that starts the record for the given track as an argument
    func startRecord(track: String) {
        let fileUrl = getFileUrl(file: track)
        do {
            deleteFile(file: track)
            recorder = try AVAudioRecorder(url: fileUrl, settings: settings)
            recorder!.isMeteringEnabled = true
            recorder!.delegate = self
            recorder!.prepareToRecord()
            recorder!.record()
            //print("CURRENT RECORD TIME: \(String(describing: recorder?.currentTime))")
            //  print("player 2 \(playerNode2.lastRenderTime?.audioTimeStamp)")
        } catch let error {
            print("Error starting record: \(error.localizedDescription)")
        }
    }
    
    func endRecord() {
        recorder!.stop()
    }
    
    // Resets audio engine after each record and playback
    func resetAudioSetup() {
        audioEngine.stop()
        audioEngine.reset()
        
    }

    func recordTrack(withName name: String) {
        disablePlayStop()
        if name == "track2" {
            labelTrack2.textColor = Colors.colorRecord
            labelTrack2.text = "Recording"
            labelTrack2.startBlink()
            track2Recording = true
            
            resetAudioSetup()
//            audioEngine.detach(playerNode1)
            audioSetup()
            prepareStartEngine()
            prepareAudioPlay(track: "track1", audioPlayerNode: playerNode1)
            startRecord(track: "track2")
           // print("player 1 \(playerNode1.lastRenderTime)")

        } else {
            disablePlayStop()
          
            labelTrack1.textColor = Colors.colorRecord
            labelTrack1.text = "Recording"
            labelTrack1.startBlink()
            track1Recording = true
            
            resetAudioSetup()
//               audioEngine.detach(playerNode2)
            audioSetup()
            prepareStartEngine()
            prepareAudioPlay(track: "track2", audioPlayerNode: playerNode2)
            startRecord(track: "track1")
            //  print("player 2 \(playerNode2.lastRenderTime)")
        }
      
    }
    
    func stopRecording(track: String) {
        if track == "track2" {
            finish()
            track1Recording = false
            labelTrack1.text = getCurrentDate()
            labelTrack1.stopBlink()
        } else {
            finish()
            track2Recording = false
            labelTrack2.text = getCurrentDate()
            labelTrack2.stopBlink()
        }
    }
    
    // Calls a function that enables the top buttons
    // And ens the current recording
    func finish() {
        enablePlayStop()
        endRecord()
    }
    
    // Resets everything to it's initial state with deleting the records
    func deleteAllTracks() {
        disablePlayStop()
        labelTrack1.text = "No Record"
        labelTrack2.text = "No Record"
        labelTrack1.textColor = Colors.colorNoRecord
        labelTrack2.textColor = Colors.colorNoRecord
        labelTrack1.stopBlink()
        labelTrack2.stopBlink()
        track1Recording = false
        track2Recording = false
        resetAudioSetup()
        deleteFile(file: "track1")
        deleteFile(file: "track2")
    }
    
    
    // MARK: - Actions
    
    @IBAction func actionButtonPlayPause(_ sender: Any) {
        // This part is for testing the current version
        // If this code succeds then it must be mixed with the code above
        resetAudioSetup()
        audioSetup()
        prepareStartEngine()
        prepareAudioPlay(track: "track1", audioPlayerNode: playerNode1)
        prepareAudioPlay(track: "track2", audioPlayerNode: playerNode2)
    }
    
    @IBAction func actionButtonStop(_ sender: Any) {
        playAllPlaying = false
        buttonPlayPause.setTitle("PLAY", for: .normal)
        resetAudioSetup()
    }
    
    @IBAction func actionButtonRecordTrack1(_ sender: Any) {
        if track2Recording { stopRecording(track: "track2") }
        if track1Recording {
            stopRecording(track: "track2")
        } else {
            recordTrack(withName: "track1")
        }
    }
    
    @IBAction func actionButtonRecordTrack2(_ sender: Any) {
        if track1Recording { stopRecording(track: "track1") }
        if track2Recording {
            stopRecording(track: "track1")
        } else {
            recordTrack(withName: "track2")
        }
    }
    
    @IBAction func actionButtonResetTracks(_ sender: Any) {
        // Make a popup which asks to delete all and reset
        let alertController = UIAlertController(title: "Reset", message: "You are about to reset the records.", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: "Reset", style: .destructive, handler: { (_) in
            self.deleteAllTracks()
            self.resetAudioSetup()
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    // MARK: - File Management
    
    func getFileUrl(file: String) -> URL {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as URL
        let soundUrl = documentDirectory.appendingPathComponent(file + ".m4a")
        return soundUrl
    }
    
    func deleteFile(file: String) {
        let fileManager = FileManager.default
        let filePath = getFileUrl(file: file).path
        if fileManager.fileExists(atPath: filePath){
            do {
                try fileManager.removeItem(atPath: filePath)
            } catch {
                if let err = error as Error? {
                    print("FileManager Error \(err.localizedDescription)")
                }
            }
        } else {
          // print("FileManager Error \(filePath) does not exist")
        }
    }
    
    func getCurrentDate() -> String {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let date = Date()
        return dateFormatter.string(from: date)
    }
    
    // MARK: - Permision
    
    func checkMicrophonePermission() {
        // AVAudioSession is the main controller of audio input and output
        // We need this in order to stop play, record and stop other audio playing apps
        audioSession = AVAudioSession.sharedInstance()
        do {
            // We need to tell the audio session that we need both playing and recording
            try audioSession.setCategory(.playAndRecord, mode: .default)
            
            // Set the minimal buffer duration for minimal delay
            // It's written that 0.0053 is the minimal duration in Apple docs
            try audioSession.setPreferredIOBufferDuration(0.0053)
            
            // And activate our session
            try audioSession.setActive(true)
            
            // Get the input latency and set it as text for lower left corner of UI (we will need this later)
           // buttonInputLatency.title = String(format: "%.3f", audioSession.inputLatency)
            
            // Get the output latency and set it as text for lower left corner of UI
         //   buttonOutputLatency.title = String(format: "%.3f", audioSession.outputLatency)
            
            // We need audio permission from iOS to continue
            audioSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        // This part is where the permisssion is granted
                        // For now we remove all the recorded tracks
                        self.deleteAllTracks()
                    } else {
                        // If not allowed show a message popup ask for it again
                        // one button to try again
                        // and one button to go to settings and there turn the permission on
                        let alertController = UIAlertController(title: "Audio Permission", message: "In order for the app to work you must enable the microphone permission. To do that go to app settings and turn it on.", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Try Again", style: .default, handler: {(_) in
                            self.checkMicrophonePermission()
                        }))
                        alertController.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (_) in
                            if let url = URL(string:UIApplication.openSettingsURLString) {
                                if UIApplication.shared.canOpenURL(url) {
                                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                }
                            }
                        }))
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            }
        } catch {
            // Failed to get audio session
            // in this case it might be an iOS bug
        }
    }
    
        
    
}
