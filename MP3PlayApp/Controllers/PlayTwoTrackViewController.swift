////
////  PlayTwoTrackViewController.swift
////  Test23
////
////  Copyright © 2019 Agency Canoe. All rights reserved.
////
//
//import UIKit
//import AVFoundation
//
//class PlayTwoTrackViewController: UIViewController, AVAudioRecorderDelegate {
//
//    @IBOutlet weak var record1Button: UIButton!
//    @IBOutlet weak var record2Button: UIButton!
//    @IBOutlet weak var playAllButton: UIButton!
//    
//    // MARK: - Properties
//    
//    var recordingSession: AVAudioSession!
//    var audioRecorder: AVAudioRecorder!
//    var engine = AVAudioEngine()
//    
//    var previousIndex = 0
//    var currentIndex = 0
//    
//    var customPlayers = [CustomPlayerNode]()
//    
//
//    // MARK: - View Lifecyle
//    
//    override func viewDidLoad() {
//        super.viewDidLoad() 
//        audioSessionConfiguration()
//        let customPlayer1 = CustomPlayerNode(player: AVAudioPlayerNode(), tag: 0, isAttached: false)
//        let customPlayer2 = CustomPlayerNode(player: AVAudioPlayerNode(), tag: 1, isAttached: false)
//        
//        customPlayers.append(customPlayer1)
//        customPlayers.append(customPlayer2)
//
//    }
//    
//    // MARK: - Actions
//    
//    @IBAction func record1Action(_ sender: UIButton) {
//         recordAudioFile(index: 0)
//    }
//    
//    @IBAction func record2Action(_ sender: UIButton) {
//         recordAudioFile(index: 1)
//    }
//    
//    func recordAudioFile(index: Int) {
//        previousIndex = currentIndex
//        currentIndex = index
//
//        if audioRecorder == nil {
//             playAllOtherAudioFiles()
//             startRecording(index: currentIndex)
//        } else {
//            finishRecording(index: previousIndex, success: true)
//            if currentIndex != previousIndex {
//                playAllOtherAudioFiles()
//                startRecording(index: currentIndex)
//            }
//        }
//    }
//    
//    func playAllOtherAudioFiles() {
//        resetAudioEngine()
//        for playerData in customPlayers {
//            if playerData.tag != currentIndex {
//                if playerData.audioFile != nil {
//                    play(index: playerData.tag)
//                }
//            }
//        }
//    }
//    
//    
//    @IBAction func playAllAction(_ sender: UIButton) {
//        finishRecording(index: currentIndex, success: true)
//        resetAudioEngine()
//        play(index: 0)
//        play(index: 1)
//    }
//    
//    // MARK: - Record
//    
//    func audioSessionConfiguration() {
//        recordingSession = AVAudioSession.sharedInstance()
//        
//        do {
//            try recordingSession.setCategory(.playAndRecord, mode: .default)
//            try recordingSession.setActive(true)
//            recordingSession.requestRecordPermission() { allowed in
//                DispatchQueue.main.async {
//                    if allowed {
//                    } else {
//                        // failed to record!
//                    }
//                }
//            }
//        } catch {
//            // failed to record!
//        }
//    }
//    
//    func startRecording(index: Int) {
//        
//        let audioName = "recording" + String(index) + ".m4a"
//        let audioFilename = getDocumentsDirectory().appendingPathComponent(audioName)
//       
//        let settings = [
//            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
//            AVSampleRateKey: 12000,
//            AVNumberOfChannelsKey: 1,
//            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
//        ]
//    
//        do {
//            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
//            audioRecorder.delegate = self
//            audioRecorder.record()
//            
//            if currentIndex == 0 {
//                record1Button.setTitle("recording ... ", for: .normal)
//            } else {
//                record2Button.setTitle("recording ... ", for: .normal)
//            }
//        } catch {
//          // finishRecording(success: false)
//            print("FAILED")
//        }
//    }
//    
//    func getDocumentsDirectory() -> URL {
//        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
//        print(paths[0])
//        return paths[0]
//    }
//    
//    func finishRecording(index: Int, success: Bool) {
//        if audioRecorder != nil {
//            audioRecorder.stop()
//            audioRecorder = nil
//        }
//        
//        
//        if let audioFile = prepareAudioFile(index: index) {
//            
//            for player in customPlayers {
//                if player.tag == index {
//                    player.audioFile = audioFile
//                }
//            }
//            
//            if index == 0 {
//                record1Button.setTitle("STOP", for: .normal)
//            } else if index == 1 {
//                 record2Button.setTitle("STOP", for: .normal)
//            }
//        }
//        
//    }
//
//    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
//        if !flag {
//           // finishRecording(success: false)
//        }
//    }
//    
//    // MARK: - Play
//    
//    func play(index: Int) {
//       
//     
//        // attach player to engine
//        
//        for  playerData in customPlayers {
//            if index == playerData.tag {
//                engine.attach(playerData.player)
//                engine.connect(playerData.player, to: engine.mainMixerNode, format: playerData.audioFile?.processingFormat)
//                // setup player
//                // provide audio file url
//                guard let audioFile = playerData.audioFile else { return }
//                playerData.player.scheduleFile(audioFile, at: nil, completionHandler: nil)
//                playerData.isAttached = true
//            }
//        }
//        
//        // prepare and start engine
//        engine.prepare()
//        try! engine.start()
//        
//        for  player in customPlayers {
//            if index == player.tag {
//               player.player.play()
//            }
//        }
//    }
//    
//    func resetAudioEngine() {
//        for (_, player) in customPlayers.enumerated() {
//            if player.isAttached {
//                player.player.stop()
//                player.player.reset()
//                engine.detach(player.player)
//                engine.stop()
//                engine.reset()
//                player.isAttached = false
//            }
//        }
//    }
//        
//    func prepareAudioFile(index: Int) -> AVAudioFile? {
//        
//        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
//        print(paths[0])
//        let audioPath =  paths[0].appendingPathComponent("recording" + String(index) + ".m4a")
//        var audioFile: AVAudioFile?
//        do {
//            audioFile = try AVAudioFile(forReading: audioPath)
//        } catch (let error) {
//            print(error)
//        }
//        return audioFile
//    }
//    
//}
//
