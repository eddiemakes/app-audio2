//
//  PlayViewController.swift
//  MP3PlayApp
//
//  Copyright © 2019 Agency Canoe. All rights reserved.
//

import UIKit
import AVFoundation

class PlayViewController: UIViewController {
    
    @IBOutlet var playerViews: [PlayView]!
    
    var array = ["Perc", "Bass Synth", "Click", "Guitar 1", "Guitar 2", "Keys 1", "Bass", "Keys 2", "FX", "Guitar Pad", "Vox FX", "Cues", "Pad", "Piano", "Drums"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for (index, view) in playerViews.enumerated() {
            print(array[index])
            view.play(fileName: array[index])
            view.button.setTitle(array[index], for: .normal)
            
        }
        
        playerViews.forEach {
            $0.switchState()
        }
    }
    
    @IBAction func playAction(_ sender: UIButton) {
        playerViews.forEach {
            $0.player.play()
            $0.button.backgroundColor = .green
        }
    }
    
    @IBAction func stopAction(_ sender: UIButton) {
        playerViews.forEach {
            $0.player.stop()
            $0.button.backgroundColor = .red
        }
    }
    
    
}

class PlayView: UIView {
    
    var button: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
        
    }()
    
    var player = AVAudioPlayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubview(button)
        setConstraints()
        button.addTarget(self, action: #selector(switchState), for: .touchUpInside)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 9)
        
    }
    
    func setConstraints() {
        button.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        button.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        button.topAnchor.constraint(equalTo: topAnchor).isActive = true
        button.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    public func play(fileName: String) {
        let path = Bundle.main.path(forResource: fileName, ofType: "mp3")
        let soundUrl = URL(fileURLWithPath: path!)
        
        do {
            try player = AVAudioPlayer(contentsOf: soundUrl)
            player.prepareToPlay()
        }
        catch let err as NSError{
            print(err.debugDescription)
        }
    }
    
    @objc func switchState() {
        if player.isPlaying {
            player.stop()
            button.backgroundColor = .red
        } else {
            player.play()
            button.backgroundColor = .green
        }
    }

}
