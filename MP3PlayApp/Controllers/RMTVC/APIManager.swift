//
//  APIManager.swift
//  MP3PlayApp
//
//  Created by Mustafa on 6/4/20.
//  Copyright © 2020 teodora. All rights reserved.
//

import Foundation
import Alamofire

class APIManager {
	
	// MARK: Download
	
	let downloadURL = "https://audioapp.subtitlemyvideo.com/app-get-song"
	
	func download(titleData: Data, completion: @escaping (String) -> Void) {
		Alamofire.upload(multipartFormData: { (multipartFormData) in
			multipartFormData.append(titleData, withName: "titleSlug")
		}, to: downloadURL, encodingCompletion: { (result) in
			
			switch result {
			case .failure(let error):
//				completion(.failure("Error you want to throw"))
				print(error.localizedDescription)
			case .success(let download, _, _):
				download.responseJSON { response in
					if let error = response.error {
						print(error.localizedDescription)
//						completion(.failure("Error you want to throw"))
					} else {
						print(response)
						self.parseJSON(response: response) { note in
							AudioFilesData.note = note
							print("NOTE SAVED")
							completion("All Data is Saved")
						}
					}
					
				}
			}
			
		})
		
	}
	
	func parseJSON(response: DataResponse<Any>, completion: @escaping (String) -> Void) {
		guard let result = response.result.value,
			let JSON = result as? NSDictionary,
			let data = JSON["data"] as? NSDictionary,
			let chunkData = data["chunk_data"] as? Array<NSDictionary>,
			let audioFiles = data["audio_files"] as? Array<NSDictionary> else {
				print("Failed to parse JSON")
				return
		}
		
		// Reset data
		AudioFilesData.audioFilesURLs.removeAll()

		for audioFile in audioFiles {
			let audioURL = audioFile["audioFileURL"] as! String
			let musicFilename = (URL(string: audioURL)?.lastPathComponent.split(separator: "-")[2])!
			let ready = String(musicFilename).replacingOccurrences(of: "m4a.", with: ".")
			
			let audioTitle = audioFile["audioFileTitle"] as? String ?? String(musicFilename).replacingOccurrences(of: "m4a.m4a", with: "")
			AudioFilesData.audioFilesTitles.append(audioTitle)
			
			self.dowloadAudiofile(fromURL: audioURL, withName: ready) { response in
				print(response)
				print(chunkData[0]["note"]!)
				completion(chunkData[0]["note"]! as! String)
			}
		}
	}
	
	func dowloadAudiofile(fromURL audioURL: String, withName musicFilename: String, completion: @escaping (String) -> Void) {
		let destination: DownloadRequest.DownloadFileDestination = { _, _ in
			var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
			documentsURL.appendPathComponent(musicFilename)
			print(documentsURL)
			AudioFilesData.audioFilesURLs.append(documentsURL)
			return (documentsURL, [.removePreviousFile])
		}
		
		Alamofire.download(audioURL, to: destination)
			.response() { response in
				completion("AudioFiles files Downloaded")
		}
	}

// MARK: - Upload
	
	let uploadUrl = "https://audioapp.subtitlemyvideo.com/app-audio-save"
	
	func upload(titleData: Data, tracksNames: [String], audioDataArray: [Data], lyricsData: Data) {
		
		Alamofire.upload(multipartFormData: { (multipartFormData) in
			for (index, audioData) in audioDataArray.enumerated() {
				multipartFormData.append(audioData, // the audio as Data
					withName: "Filedata" + String(index),
					fileName: "file" + String(index+1) + ".m4a", // name of the file
//					fileName: tracksNames[index], // name of the file
					mimeType: "audio/mpeg")
			}
						
			for (index ,value) in tracksNames.enumerated() {
				multipartFormData.append(("file" + String(index+1) + ".m4a").data(using: String.Encoding.utf8)!, withName: "audioFileData[\(index)][fileName]")
				
				multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: "audioFileData[\(index)][title]")
			}
			
			multipartFormData.append(titleData, withName: "titleSlug")
			multipartFormData.append(lyricsData, withName: "note")
		}, to: uploadUrl, encodingCompletion: { (result) in
			
			switch result {
			case .failure(let error):
				//				completion(.failure("Error you want to throw"))
				print(error.localizedDescription)
			case .success(let upload, _, _):
				upload.uploadProgress(closure: { (progress) in
					print("Upload Progress: \(progress.fractionCompleted)")
				})
				
				upload.responseJSON { response in
					if let error = response.error {
						print(error.localizedDescription)
						//						completion(.failure("Error you want to throw"))
					} else {
						print(response)
						//						completion(.success(()))
					}
				}
			}
			
		})
		
	}
}
