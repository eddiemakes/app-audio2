//
//  RMTVC+Realm.swift
//  MP3PlayApp
//
//  Created by Mustafa on 3/4/20.
//  Copyright © 2020 teodora. All rights reserved.
//

import UIKit

extension RecordMultipleTracksViewController {
	
	func saveToRealm(trackName: String) {
		// Save the Track
		let trackResults = realm.objects(Track.self)
		
		let trackFileRealm = Track()
		trackFileRealm.main = trackResults.count + 1
		trackFileRealm.titleSlug = trackName
		trackFileRealm.note = noteTextView.text
		
		try! realm.write {
			realm.add(trackFileRealm)
		}
		
		// Save the sudio files
		let audioResults = realm.objects(AudioFile.self)
		
		for (index, player) in customPlayers.enumerated() {
			let audioFileRealm = AudioFile()
			audioFileRealm.main = audioResults.count + 1
			audioFileRealm.chunkMain = trackFileRealm.main
			audioFileRealm.audioFileTitle = getTrackTitle(at: index)!
			guard let audioFile = player.audioFile else {continue}
			audioFileRealm.audioFileURL = audioFile.url.absoluteString
			try! realm.write {
				realm.add(audioFileRealm)
			}
		}
		
		print(realm.configuration.fileURL!)
	}
	
}
