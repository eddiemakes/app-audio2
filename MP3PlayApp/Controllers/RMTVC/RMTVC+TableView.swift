//
//  RMTVC+TableView.swift
//  MP3PlayApp
//
//  Created by Mustafa on 3/4/20.
//  Copyright © 2020 teodora. All rights reserved.
//

import UIKit

extension RecordMultipleTracksViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 100
	}
}

extension RecordMultipleTracksViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return customPlayers.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "RecordCellID", for: indexPath) as! RecordTrackTableViewCell
		cell.isEmpty = true
		cell.trackTitleTextField.text = "Track \(indexPath.row + 1)"
		cell.buttonAction = { [weak self] sender in
			guard let `self` = self else { return }
			self.recordAudioFile(index: indexPath.item)
			self.updateCellUI()
		}
		cell.selectionStyle = .none
		
		return cell
	}
	
}

extension RecordMultipleTracksViewController {
	func setupTableFooter() -> UIView {
		let footer = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 167))
		let textView = UITextView(frame: CGRect(x: 10, y: 10, width: footer.frame.width - 20, height: footer.frame.height - 20))
		textView.font = .systemFont(ofSize: 14)
		textView.borderWidth = CGFloat(1)
		noteTextView = textView
		footer.addSubview(textView)
		footer.layoutIfNeeded()
		return footer
	}
}
