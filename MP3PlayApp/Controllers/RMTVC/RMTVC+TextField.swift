//
//  RMTVC+TextField.swift
//  MP3PlayApp
//
//  Created by Mustafa on 6/4/20.
//  Copyright © 2020 teodora. All rights reserved.
//

import UIKit

extension RecordMultipleTracksViewController: UITextFieldDelegate {
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if !isValidCharacter(character: string) {
			return false
		}
		return true
	}
}
