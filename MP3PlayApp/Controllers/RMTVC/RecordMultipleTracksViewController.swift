//
//  RecordMultipleTracksViewController.swift
//  MP3PlayApp
//
//  Copyright © 2019 Agency Canoe. All rights reserved.
//

import UIKit
import AVFoundation
import RealmSwift

class RecordMultipleTracksViewController: UIViewController, AVAudioRecorderDelegate {

    @IBOutlet weak var customNavigationBar: UIView!
    @IBOutlet weak var openSavedTracksButton: UIButton!
    @IBOutlet weak var saveRecoredTracksButton: UIButton!
    @IBOutlet weak var tracksFIleNameTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    var currentIndex: Int = -1
    var previousIndex: Int = -1
	var isRecording = false
    var engine = AVAudioEngine()
    var customPlayers = [CustomPlayerNode]()
    var currentFileNameNumber: String = ""
	weak var noteTextView: UITextView!
	
	// MARK: - Realm Properties
	lazy var realm = try! Realm()

    // MARK: - AVFoundation Properties
    
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    
    // MARK: - View Lifecyle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tracksFIleNameTextField.delegate = self
        tableViewDataSource()
		tableView.tableFooterView = setupTableFooter()
        audioSessionConfiguration()
    }
    
    func tableViewDataSource() {
        let customPlayer1 = CustomPlayerNode(player: AVAudioPlayerNode(), tag: 0, isAttached: false)
        let customPlayer2 = CustomPlayerNode(player: AVAudioPlayerNode(), tag: 1, isAttached: false)
        let customPlayer3 = CustomPlayerNode(player: AVAudioPlayerNode(), tag: 2, isAttached: false)
        
        customPlayers.append(customPlayer1)
        customPlayers.append(customPlayer2)
        customPlayers.append(customPlayer3)

    }
    
    
    // MARK: - Actions
    
    @IBAction func saveRecordedTrackAction(_ sender: UIButton) {
		guard let trackName = tracksFIleNameTextField.text else { return }
		guard isTrackNameValid(trackName: trackName) else { return }
		
		print(TrackNameValidation.Valid.rawValue)
        currentFileNameNumber = tracksFIleNameTextField.text!
        createFolderWithInputName(name: currentFileNameNumber)
		dismissKeyboard()
		
		// Save to Realm
		saveToRealm(trackName: trackName)
		
		// Upload to Server
		uploadToServer(trackName: trackName)
		
		showAlert(withTitle: "Track Saved Successfully",
				  message: "")
    }
	
	@IBAction func openSavedTracksAction(_ sender: UIButton) {
		print("Open button Tapped")
		
		// Wipe any previous loaded data
		resetData()
		
		// Download from server
		let apiManager = APIManager()
		let titleData = tracksFIleNameTextField.text!.data(using: .utf8)!
		apiManager.download(titleData: titleData) { response in
			print(response)
			self.updatePlayers()
			self.updateUI()
		}
		
	}
	
    @IBAction func fileNameTextFieldEditingDidEnd(_ sender: UITextField) {
        if let text = sender.text {
            currentFileNameNumber = text
        }
      
    }
    
    // MARK: - Audio Session Setup
    
    func audioSessionConfiguration() {
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { allowed in
                DispatchQueue.main.async {
                    if allowed {
                        
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
    }
    
	// MARK: - Record
	
    func startRecording(index: Int) {
        
//        let audioName = "audio_file_" + String(index) + ".m4a"
		print("Track title at", index)
		guard let trackTitle = getTrackTitle(at: index) else { return }
		let audioName = trackTitle + ".m4a"
        let audioFilename = getDocumentsDirectory().appendingPathComponent(audioName)
		AudioFilesData.audioFilesURLs.append(audioFilename)
        
		let settings: [String: Any] = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000, //44100
            AVNumberOfChannelsKey: 1, //2
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue,
			AVEncoderBitRatePerChannelKey: 19200, // 192kbps
			AVEncoderBitRateStrategyKey: AVAudioBitRateStrategy_Constant
//            AVLinearPCMBitDepthKey: 16,
//            AVLinearPCMIsBigEndianKey: 0,
//            AVLinearPCMIsFloatKey: 0
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
			print("Audio Recorder Initialised")
            audioRecorder.delegate = self
//            audioRecorder.prepareToRecord()
//            audioRecorder.isMeteringEnabled = true
            audioRecorder.record()
			isRecording = true
			print("RECORDING")

        } catch {
            // finishRecording(success: false)
            print("FAILED")
			print(error.localizedDescription)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
//        print(paths[0])
        return paths[0]
    }
    
    func finishRecording(index: Int, success: Bool) {
        if audioRecorder != nil {
            audioRecorder.stop()
			isRecording = false
            audioRecorder = nil
        }
        
        if let audioFile = prepareAudioFile(index: index) {
            for player in customPlayers {
                if player.tag == index {
                    player.audioFile = audioFile
					print("Audio File Created")
                }
            }
        }

    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
             finishRecording(index: previousIndex, success: false)
        }
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
		if let error = error {
			print(error.localizedDescription)
		}
    }
    
    func createFolderWithInputName(name: String) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let dataPath = documentsDirectory.appendingPathComponent(currentFileNameNumber)
        
        do {
            try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print("Error creating directory: \(error.localizedDescription)")
        }

    }
    
    func findCurrentDirectory() -> URL? {
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        do {
            let fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            print(fileURLs)
            return fileURLs.first
//            for file in fileURLs {
//                if file.lastPathComponent == currentFileNameNumber {
//                     return file
//                }
//            }
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
        
        return nil
    }
    
    func prepareAudioFile(index: Int) -> AVAudioFile? {
        
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
		//        let audioPath =  path?.appendingPathComponent("audio_file_" + String(index) + ".m4a")
//		guard let trackTitle = getTrackTitle(at: index) else { return nil }
//		let audioName = trackTitle + ".m4a"
//		let audioName = "audiofile" + String(index) + ".m4a"
//		let audioPath =  path?.appendingPathComponent(audioName)
		print("playing index",index, AudioFilesData.audioFilesURLs.count)
		let audioPath: URL? = AudioFilesData.audioFilesURLs[index]

        var audioFile: AVAudioFile?
        do {
            audioFile = try AVAudioFile(forReading: audioPath!)
			print("File", index, "Prepared")
        } catch (let error) {
            print(error)
        }
        return audioFile
    }
    
    func recordAudioFile(index: Int) {
        previousIndex = currentIndex
        currentIndex = index
        
        var delayTime = Constants.delayTimeDefault
        if let stringValue = MP3UserDefault.shared.delayTime, let value = TimeInterval(stringValue) {
            delayTime = value
        }
        if audioRecorder == nil {
            playAllOtherAudioFiles()
            DispatchQueue.main.asyncAfter(deadline: .now() + delayTime) {
				print("Start", self.currentIndex)
                self.startRecording(index: self.currentIndex)
            }
        } else {
			print("Finish", previousIndex)
            finishRecording(index: previousIndex, success: true)
            if currentIndex != previousIndex {
                playAllOtherAudioFiles()
                DispatchQueue.main.asyncAfter(deadline: .now() + delayTime) {
                    self.startRecording(index: self.currentIndex)
                }
            } else {
                previousIndex = -1
                currentIndex = -1
            }
        }
    }
    
    func updateCellUI() {
        for (index, value) in customPlayers.enumerated() {
			print("Value is ", value.audioFile)
            if index == currentIndex {
                let cell = tableView.cellForRow(at: IndexPath.init(row: index, section: 0)) as! RecordTrackTableViewCell
                cell.isRecording = true
                cell.isEmpty = false
                cell.isRecorded = false
            }
            else if value.audioFile != nil {
                let cell = tableView.cellForRow(at: IndexPath.init(row: index, section: 0)) as! RecordTrackTableViewCell
                cell.isRecording = false
                cell.isEmpty = false
                cell.isRecorded = true
            }
            else {
                let cell = tableView.cellForRow(at: IndexPath.init(row: index, section: 0)) as! RecordTrackTableViewCell
                cell.isRecording = false
                cell.isEmpty = true
                cell.isRecorded = false
            }
        }
    }
    
    func playAllOtherAudioFiles() {
        resetAudioEngine()
//        for playerData in customPlayers {
//            if playerData.tag != currentIndex {
//                if playerData.audioFile != nil {
//                    play(index: playerData.tag)
//                }
//            }
//        }
        
        let players = customPlayers.filter { player in
            player.tag != currentIndex && player.audioFile != nil
        }
		print(customPlayers.count)
		print("Data" ,AudioFilesData.audioFilesURLs.count)
		for pp in customPlayers {
			print(pp.audioFile?.url)
		}
		print(players.count)
        if !players.isEmpty {
            for player in players {
                engine.attach(player.player)
                engine.connect(player.player, to: engine.mainMixerNode, format: player.audioFile?.processingFormat)
                // setup player
                // provide audio file url
				print(player.audioFile?.url)
                player.player.scheduleFile(player.audioFile!, at: nil, completionHandler: nil)
                player.isAttached = true
            }
            // prepare and start engine
            try! engine.start()
            for player in players {
                player.player.play()
            }
        }
    }
    
    // MARK: - Play
    
    func play(index: Int) {
        
        // attach player to engine
        for playerData in customPlayers {
            if index == playerData.tag {
                engine.attach(playerData.player)
                engine.connect(playerData.player, to: engine.mainMixerNode, format: playerData.audioFile?.processingFormat)
                // setup player
                // provide audio file url
                guard let audioFile = playerData.audioFile else { return }
                playerData.player.scheduleFile(audioFile, at: nil, completionHandler: nil)
                playerData.isAttached = true
            }
        }
        
        // prepare and start engine
        engine.prepare()
        try! engine.start()
        
        for player in customPlayers {
            if index == player.tag {
                player.player.play()
            }
        }
    }
    
    func resetAudioEngine() {
        for (_, player) in customPlayers.enumerated() {
            if player.isAttached {
                player.player.stop()
                player.player.reset()
                engine.detach(player.player)
                player.isAttached = false
            }
        }
        engine.stop()
        engine.reset()
    }
    
    @IBAction func playAllAction(_ sender: UIButton) {
		if isRecording {
			finishRecording(index: currentIndex, success: true)
		}
        previousIndex = -1
        currentIndex = -1
        updateCellUI()
        playAllOtherAudioFiles()
    }
    
    @IBAction func stopAllAction(_ sender: UIButton) {
        resetAudioEngine()
        finishRecording(index: currentIndex, success: true)
        previousIndex = -1
        currentIndex = -1
        updateCellUI()
    }
    
    
}

//class AudioPlayer {
//
//    fileprivate var audioEngine: AVAudioEngine!
//    fileprivate var audioPlayer: AVAudioPlayerNode!
//    fileprivate let bus = 0
//
//
//    // MARK: - Play Audio
//
//    func play(audioUrl: URL) throws {
//
////        let AVBufferSize = 1024
//        let session = AVAudioSession.sharedInstance()
//        try session.setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)), mode: AVAudioSession.Mode.default)
//
//        audioEngine = AVAudioEngine()
//        audioPlayer = AVAudioPlayerNode()
//
////        let audioFile = try AVAudioFile(forReading: audioUrl)
//        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//        let audioPath =  path?.appendingPathComponent("recording" + String(0) + ".m4a")
//        let audioFile = try AVAudioFile(forReading: audioPath!)
//
//        audioEngine.attach(audioPlayer)
//        audioEngine.connect(audioPlayer, to: audioEngine.outputNode, format: audioFile.processingFormat)
//
//        audioPlayer.scheduleFile(audioFile, at: nil, completionHandler: nil)
//
//        // right now this one is not used, it could be used to draw signal level, send current position of playing
////        audioEngine.outputNode.installTap(onBus: bus, bufferSize: AVAudioFrameCount(AVBufferSize), format: nil) {
////            buffer, time in
////        }
//
//        audioEngine.prepare()
//        try audioEngine.start()
//
//        audioPlayer.play()
//    }
//
//
//    func stop() {
//        if audioPlayer != nil {
//            audioPlayer.stop()
//            audioPlayer = nil
//        }
//        if audioEngine != nil {
//            audioEngine.stop()
//            audioEngine.reset()
//            audioEngine = nil
//        }
//    }
//
//}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
    return input.rawValue
}

extension RecordMultipleTracksViewController {
	
	func isTrackNameValid(trackName: String) -> Bool {
		if trackName.isEmpty {
			showAlert(withTitle: TrackNameValidation.Invalid.rawValue,
					  message: TrackNameValidation.IsEmpty.rawValue)
			return false
		}
		
		if trackName.count > 20 {
			showAlert(withTitle: TrackNameValidation.Invalid.rawValue,
					  message: TrackNameValidation.IsMoreThan20.rawValue)
			return false
		}
		
		return true
	}
	
	func isValidCharacter(character: String) -> Bool {
		let nameRegex = "^[a-zA-Z0-9_]*$"
		return NSPredicate(format: "SELF MATCHES %@", nameRegex).evaluate(with: character)
	}
	
	private func uploadToServer(trackName: String) {
		let apiManager = APIManager()
		var tracksNames = [String]()
		var audioDataArray = [Data]()
		for (index, player) in customPlayers.enumerated() {
			guard let audioFile = player.audioFile else {continue}
			tracksNames.append(getTrackTitle(at: index)!)
			audioDataArray.append(try! Data(contentsOf: player.audioFile!.url))
		}
		let titleData = trackName.data(using: .utf8)!
		let lyricsData = noteTextView.text.data(using: .utf8)!
		
		apiManager.upload(titleData: titleData, tracksNames: tracksNames, audioDataArray: audioDataArray, lyricsData: lyricsData)
	}
	
	func updateUI() {
		DispatchQueue.main.async {
			self.noteTextView.text = AudioFilesData.note
			self.setTrackTitles()
			self.updateCellUI()
		}
	}
	
	func updatePlayers() {
		customPlayers.removeAll()
		print("Audio Files",  AudioFilesData.audioFilesURLs)
		for (index, _) in AudioFilesData.audioFilesURLs.enumerated() {
			let audioFile = prepareAudioFile(index: index)
			customPlayers.append(CustomPlayerNode(player: AVAudioPlayerNode(), audioFile: audioFile, tag: index, isAttached: false, volume: 85))
			print("Player", customPlayers.last?.audioFile?.url)
		}
	}
	
	func getTrackTitle(at index: Int) -> String? {
		let indexPath = IndexPath(row: index, section: 0)
		guard let cell = tableView.cellForRow(at: indexPath) as! RecordTrackTableViewCell? else {
			return nil
		}
		let trackTitle = cell.trackTitleTextField.text!
		return trackTitle
	}
	
	func setTrackTitles() {
		for (index, trackTitle) in AudioFilesData.audioFilesTitles.enumerated() {
			let indexPath = IndexPath(row: index, section: 0)
			guard let cell = tableView.cellForRow(at: indexPath) as! RecordTrackTableViewCell? else {
				return
			}
			cell.trackTitleTextField.text = trackTitle
		}
	}
	
	func resetData() {
		AudioFilesData.audioFilesTitles.removeAll()
		AudioFilesData.audioFilesURLs.removeAll()
		AudioFilesData.note.removeAll()
	}
	
}
