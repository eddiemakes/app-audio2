//
//  RecordTrackTableViewCell.swift
//  MP3PlayApp
//
//  Copyright © 2019 Agency Canoe. All rights reserved.
//

import UIKit
import AVFoundation

class RecordTrackTableViewCell: UITableViewCell, AVAudioRecorderDelegate {

    @IBOutlet weak var volumeLabel: UILabel!
    @IBOutlet weak var volumeTextField: UITextField!
    @IBOutlet weak var recordIndicatorView: UIView!
    @IBOutlet weak var trackTitleTextField: UITextField!
    
    var isRecording: Bool = false {
        didSet { updateUI() }
    }
	
    var isRecorded: Bool = false {
		didSet { updateUI() }
    }
	
    var isEmpty: Bool = true {
        didSet { updateUI() }
    }
    
	var buttonAction: ((Any) -> Void)?
   
    func updateUI() {
        if isRecording {
            recordIndicatorView.layer.borderColor = UIColor.clear.cgColor
            recordIndicatorView.layer.borderWidth = 0.0
            recordIndicatorView.backgroundColor = .red
        } else {
            recordIndicatorView.layer.cornerRadius = recordIndicatorView.frame.size.width/2
            recordIndicatorView.layer.borderWidth = 2.0
            recordIndicatorView.layer.borderColor = UIColor.black.cgColor
            recordIndicatorView.backgroundColor = .white
            if isRecorded {
				recordIndicatorView.backgroundColor = Colors.themeColor
            }
        }
    }
    
    // MARK: - Actions

    @IBAction func volumeChangedAction(_ sender: UITextField) {
   
    }
    
	@IBAction func recordButtonTapped(_ sender: Any) {
		self.buttonAction?(sender)
	}
	
	// MARK: - Private Setup
    
    func setVolumeValue() {
        
    }
    
    
    func setRecordIndicatorUI() {
       
    }
    
    // MARK: - Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setVolumeValue()
		trackTitleTextField.delegate = self
		// Add padding to TextView
		let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 0))
		trackTitleTextField.leftView = paddingView
		trackTitleTextField.rightView = paddingView
		trackTitleTextField.leftViewMode = .always
		trackTitleTextField.rightViewMode = .always
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       
    }

}

extension RecordTrackTableViewCell: UITextFieldDelegate {
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if !isValidCharacter(character: string) {
			return false
		}
		return true
	}
	
	func isValidCharacter(character: String) -> Bool {
		let nameRegex = "^[a-zA-Z0-9_ ]*$"
		return NSPredicate(format: "SELF MATCHES %@", nameRegex).evaluate(with: character)
	}
}
