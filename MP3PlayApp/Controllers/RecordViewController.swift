//
//  RecordViewController.swift
//  MP3PlayApp
//
//  Copyright © 2019 Agency Canoe. All rights reserved.
//

import UIKit

class RecordViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    

   

}

extension RecordViewController: UITableViewDelegate {
    
}

extension RecordViewController: UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! RecordTableViewCell
        cell.audioTitleLabel.text = "Track \(indexPath.row)"
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
}
