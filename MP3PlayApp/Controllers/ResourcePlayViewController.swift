//
//  ResourcePlayViewController.swift
//  MP3PlayApp
//
//  Copyright © 2019 Agency Canoe. All rights reserved.
//

import UIKit
import AVFoundation

class ResourcePlayViewController: UIViewController {

    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var urlTextField: UITextField!
    @IBOutlet weak var loadButton: UIButton!
    @IBOutlet weak var loadedFileLabel: UILabel!
    
    var player = AVAudioPlayer()
    var currentAudioFileName = ""
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playButton.isSelected = false
        activityIndicator.hidesWhenStopped = true
        playButton.isEnabled = false
//        deleteAudioFileFromDocuments(fileName: "")
        hideKeyboardWhenTappedAround()
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [.mixWithOthers, .allowAirPlay])
            print("Playback OK")
            try AVAudioSession.sharedInstance().setActive(true)
            print("Session is Active")
        } catch {
            print(error)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        if player != nil { // hot fix
//            if player.isPlaying {
//                player.stop()
//            }
//        }
    }
    
    func playSound(soundPath: String) {
        let sound = URL(fileURLWithPath: soundPath)
        do{
            player = try AVAudioPlayer(contentsOf: sound, fileTypeHint: "wav")
            player.numberOfLoops = -1
            player.prepareToPlay()
            player.play()
            DispatchQueue.main.async {
                self.playButton.isSelected = true
            }
            
        } catch {
            print("Error getting the audio file")
        }
    }
    
    //MARK: - Actions
    
    @IBAction func playAction(_ sender: UIButton) {
        playButton.isSelected = !playButton.isSelected
        if playButton.isSelected {
            player.play()
        } else {
            player.stop()
        }
    }
    
    @IBAction func loadAction(_ sender: UIButton) {
        guard var url = urlTextField.text else { return
            // show error message
        }
        if url.suffix(4) == "dl=0" {
           url = url.replacingOccurrences(of: "dl=0", with: "dl=1")
        }
        downloadAudioFile(urlPath: url)
        
      //  checkIfResourseExist(urlPath: "https://www.dropbox.com/s/nc0ehouw9y7vvx6/85bpm%20when%20no%20one%20else08.wav?dl=1")
        
        
    
    }
    
    func downloadAudioFile(urlPath: String) {
        // delete file
        deleteAudioFileFromDocuments(fileName: currentAudioFileName)
        
        // download new file
        guard let url = URL(string: urlPath) else { return }
        activityIndicator.startAnimating()
        currentAudioFileName = url.lastPathComponent
        
        Downloader().download(from: url) {
            if let localURL = self.getAudioFromDocumentDirectory(name: url.lastPathComponent) {
                self.playSound(soundPath: localURL)
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    self.loadedFileLabel.text = url.lastPathComponent
                    self.playButton.isEnabled = true
                }
            }
        }
    }
    
    func getAudioFromDocumentDirectory(name: String) -> String? {
        let fileManager = FileManager.default
        
        do {
            let resourceKeys : [URLResourceKey] = [.creationDateKey, .isDirectoryKey]
            let documentsURL = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let enumerator = FileManager.default.enumerator(at: documentsURL,
                                                            includingPropertiesForKeys: resourceKeys,
                                                            options: [.skipsHiddenFiles], errorHandler: { (url, error) -> Bool in
                                                                print("directoryEnumerator error at \(url): ", error)
                                                                return true
            })!
            
            for case let fileURL as URL in enumerator {
                let resourceValues = try fileURL.resourceValues(forKeys: Set(resourceKeys))
                if fileURL.lastPathComponent == name {
                    return fileURL.path
                }
                //                print(fileURL.path, resourceValues.creationDate!, resourceValues.isDirectory!)
            }
        } catch {
            print(error)
        }
        
        return nil
        
        
    }
    
    func deleteAudioFileFromDocuments(fileName: String) {
        let fileManager = FileManager.default
        
        do {
            let resourceKeys : [URLResourceKey] = [.creationDateKey, .isDirectoryKey]
            let documentsURL = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let enumerator = FileManager.default.enumerator(at: documentsURL,
                                                            includingPropertiesForKeys: resourceKeys,
                                                            options: [.skipsHiddenFiles], errorHandler: { (url, error) -> Bool in
                                                                print("directoryEnumerator error at \(url): ", error)
                                                                return true
            })!
            
            for case let fileURL as URL in enumerator {
              //  let resourceValues = try fileURL.resourceValues(forKeys: Set(resourceKeys))
//                if fileURL.lastPathComponent == fileName {
                    try? fileManager.removeItem(at: fileURL)
//                }
            }
        } catch {
            print(error)
        }
    }

}

