//
//  SettingsViewController.swift
//  MP3PlayApp
//
//  Copyright © 2019 Agency Canoe. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var delayTimeTextField: DecimalTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        delayTimeTextField.delegate = self
        delayTimeTextField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        hideKeyboardWhenTappedAround()
        
        var value = "\(Constants.delayTimeDefault)"
        if let delayTime = MP3UserDefault.shared.delayTime {
            value = delayTime
        }
        delayTimeTextField.text = value
    }
    
    @IBAction func saveAction(_ sender: Any) {
        var delayTime = "0"
        if let text = delayTimeTextField.text, !text.isEmpty {
            delayTime = text
        }
        MP3UserDefault.shared.delayTime = delayTime
		dismissKeyboard()
    }
}

extension SettingsViewController: UITextFieldDelegate {
    
    @objc func textFieldDidChange(textField: UITextField) {
        if let textField = textField as? DecimalTextField {
            textField.text = textField.handleDidChange()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let textField = textField as? DecimalTextField {
            return delayTimeTextField.shouldChangeCharactersIn(textField, replacementString: string, isOverMaxValue: false)
        }
        return true
    }
}
