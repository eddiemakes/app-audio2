//
//  AudioFile.swift
//  MP3PlayApp
//
//  Created by Mustafa on 2/4/20.
//  Copyright © 2020 teodora. All rights reserved.
//

import Foundation
import RealmSwift

class AudioFile: Object {
	@objc dynamic var main = 0
	@objc dynamic var chunkMain = 0
	@objc dynamic var audioFileTitle = ""
	@objc dynamic var audioFileURL = ""
	@objc dynamic var timeStamp = 0.0
}
