//
//  AudioFilesData.swift
//  MP3PlayApp
//
//  Created by Mustafa on 6/4/20.
//  Copyright © 2020 teodora. All rights reserved.
//

import Foundation

struct AudioFilesData {
	static var audioFilesTitles = [String]()
	static var audioFilesURLs = [URL]()
	static var note = String()
}
