//
//  Constants.swift
//  MP3PlayApp
//
//  Copyright © 2019 Agency Canoe. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
    
    // electric blue
    static let colorRecord: UIColor = UIColor(red: 0, green: 122/255, blue: 1, alpha: 1)
    
	// light gray
	static let colorNoRecord: UIColor = UIColor(white: 0.661, alpha: 2)
	
	// theme color
	static let themeColor: UIColor = UIColor(rgb: 0x76D6FF)
	
}

struct Constants {
	static let delayTimeDefault: TimeInterval = 0.09
}

enum TrackNameValidation: String {
	case Invalid = "Invalid Track Name"
	case Valid = "Track Name is Valid"
	case IsEmpty = "Please enter Track Name"
	case IsMoreThan20 = "Track Name can not be more than 20 characters"
	case InvalidCharacters = "Track Name must be AlphaNumerics or underscore only"
}
