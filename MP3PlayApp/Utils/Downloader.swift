//
//  Downloader.swift
//  MP3PlayApp
//
//  Copyright © 2019 Agency Canoe. All rights reserved.
//

import UIKit



class Downloader {
    
    func download(from fromUrl: URL, completion: @escaping () -> ()) {
       
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url: fromUrl)
     //   crateAudioFilesDirectory()
        
        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Success: \(statusCode)")
                }
                
                do {
                    let toURL = documentsUrl.appendingPathComponent(fromUrl.lastPathComponent)
                    print(toURL)
                    try FileManager.default.copyItem(at: tempLocalUrl, to: toURL)
                    completion()
                } catch (let writeError) {
                    completion()
                    // print("error writing file \() : \(writeError)")
                }
                
            } else {
               print("Failure: %@", error?.localizedDescription);
            }
        }
        task.resume()
    }
    
    
    func getDirectoryPath() -> URL? {
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("Audio Files")
        guard let url = URL(string: path) else { return nil }
        return url
    }
    
    func crateAudioFilesDirectory() {
        let fileManager = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("Audio Files")
        if !fileManager.fileExists(atPath: path) {
            try! fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
    }

}
    
    
