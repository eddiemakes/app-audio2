//
//  Extensions.swift
//  Record Twice
//
//  Copyright © 2019 Agency Canoe. All rights reserved.
//

import UIKit

extension UILabel {
    //MARK: StartBlink
    func startBlink() {
        UIView.animate(withDuration: 0.8,//Time duration
            delay:0.0,
            options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
            animations: { self.alpha = 0 },
            completion: nil)
    }
    
    //MARK: StopBlink
    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}

@IBDesignable
class DecimalTextField: UITextField {
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(copy(_:)) || action == #selector(paste(_:)) || action == #selector(select(_:)) || action == #selector(selectAll(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    override func closestPosition(to point: CGPoint) -> UITextPosition? {
        let beginning = self.beginningOfDocument
        let end = self.position(from: beginning, offset: (self.text?.count)!)
        return end
    }
    
    func shouldChangeCharactersIn(_ textField: UITextField, replacementString string: String, isOverMaxValue: Bool) -> Bool {
        if let text = textField.text {
            let char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                print("Backspace was pressed")
                return true
            } else if !CharacterSet(charactersIn: "0123456789.").isSuperset(of: CharacterSet(charactersIn: string)) {
                return false
            }
            
            if string == "." {
                guard text.range(of: ".") == nil else {
                    return false
                }
            }
            
            if isOverMaxValue {
                return false
            }
        }
        
        return true
    }
    
    func handleDidChange() -> String {
        var textString = self.text ?? ""
        if textString == "." {
            // Convert from ".3" to "0.3"
            textString = "0" + textString
        } else {
            // Remove first 0 character e.g. 08 => 8, 003.3 => 3.3
            let fullValueArr: [String] = textString.components(separatedBy: ".")
            if fullValueArr.count == 1 {
                let firstValue : String = fullValueArr[0]
                if firstValue.count > 1 {
                    if let firstCharacter = firstValue.first {
                        if firstCharacter == "0" {
                            textString.removeFirst()
                        }
                    }
                }
            } else if fullValueArr.count > 1 {
                // Remove last character e.g. 0.0888 => 0.088
                let secondValue: String = fullValueArr[1]
                if secondValue.count > 3 {
                    textString.removeLast()
                }
            }
        }
        
        return textString.handleFormartingDecimalStyle()
    }
}

extension String {
    var removedSeparator: String {
        return self.replacingOccurrences(of: ",", with: "")
    }
    
    func handleFormartingDecimalStyle() -> String {
        var newString: String = self
        let fullValueArr: [String] = self.components(separatedBy: ".")
        if fullValueArr.count > 1 {
            let firstValue: String = fullValueArr[0]
            let secondValue: String = fullValueArr[1]
            newString = firstValue.removedSeparator + "." + secondValue
        } else {
            newString = newString.removedSeparator
        }
        
        return newString
    }
}

extension UIView {
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: CGFloat {
        get {
            return CGFloat(layer.shadowOpacity)
        }
        set {
            layer.shadowOpacity = Float(newValue)
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIViewController {
	func showAlert(withTitle title: String, message: String) {
		let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
		let action = UIAlertAction(title: "OK", style: .default, handler: nil)
		alertController.addAction(action)
		DispatchQueue.main.async {
			self.present(alertController, animated: true, completion: nil)
		}
	}
}

extension UIColor {
	
	convenience init(rgb: UInt) {
		self.init(rgb: rgb, alpha: 1.0)
	}
	
	convenience init(rgb: UInt, alpha: CGFloat) {
		self.init(
			red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
			green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
			blue: CGFloat(rgb & 0x0000FF) / 255.0,
			alpha: CGFloat(alpha)
		)
	}
}
