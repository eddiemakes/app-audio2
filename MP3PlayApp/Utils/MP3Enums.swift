//
//  MP3Enums.swift
//  MP3PlayApp
//
//  Copyright © 2019 Agency Canoe. All rights reserved.
//

import Foundation

enum MP3EnumUserDefaultKeys: String {
    case delayTimeKey = "delayTimeKey"
}
