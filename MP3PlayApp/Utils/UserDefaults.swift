//
//  UserDefaults.swift
//  MP3PlayApp
//
//  Copyright © 2019 Agency Canoe. All rights reserved.
//

import Foundation

class MP3UserDefault {
    static let shared = MP3UserDefault()
    
    let userdefault = UserDefaults.standard
    func set<T>(_ key:MP3EnumUserDefaultKeys, _ value:T?) {
        userdefault.setValue(value, forKey: key.rawValue)
    }
    func get<T>(_ key: MP3EnumUserDefaultKeys) -> T? {
        return userdefault.value(forKey: key.rawValue) as? T
    }
    
    var delayTime: String? {
        set {
            set(.delayTimeKey, newValue)
        }
        get {
            return get(.delayTimeKey)
        }
    }
}
